import pika
from clickhouse_driver import Client

import config

rabbit_connection = pika.BlockingConnection(pika.ConnectionParameters(config.rabbit['host']))
channel = rabbit_connection.channel()

q_name = 'shops'
channel.queue_declare(queue=q_name)

client = Client(
    host=config.clickhouse['host'],
    user=config.clickhouse['user'],
    password=config.clickhouse['password'],
)

sql = "select distinct X_0 from CHAuchan.dtable_with_data;"
for i in client.execute_iter(sql):
    channel.basic_publish(exchange='', routing_key=q_name, body=i[0])
