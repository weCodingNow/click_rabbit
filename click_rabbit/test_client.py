import pika

import config

rabbit_connection = pika.BlockingConnection(pika.ConnectionParameters(config.rabbit['host']))
channel = rabbit_connection.channel()

q_name = 'shops'
channel.queue_declare(queue=q_name)

def callback(ch, method, properties, body):
    print(f"got {body}")

channel.basic_consume(queue=q_name, on_message_callback=callback)

channel.start_consuming()